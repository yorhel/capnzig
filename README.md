<!--
SPDX-FileCopyrightText: 2022-2023 Yoran Heling <projects@yorhel.nl>
SPDX-License-Identifier: MIT
-->

# Cap'n Proto for Zig

This is (intended to be) a pure Zig implementation of [Cap'n
Proto](https://capnproto.org/). The goal is to leverage Zig's comptime to load
Cap'n Proto schemas and create the necessary interfaces at compile time -
without adding an explicit code generation step to the build system.  Whether
that is actually possible or sensible, I've no clue. This is an experiment.

Project status: Just an incomplete parser and a struct layout algorithm,
nothing usable.

## Resources:

- https://capnproto.org/

## Alternative projects:

- https://gitlab.com/Zarzwick/capnproto-zig-alternative
