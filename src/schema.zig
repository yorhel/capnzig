// SPDX-FileCopyrightText: 2022-2023 Yoran Heling <projects@yorhel.nl>
// SPDX-License-Identifier: MIT

const std = @import("std");
const parser = @import("parser.zig");


pub const Schema = struct {
    // Tree of nodes
    tree: *const Node,
    // Creating/layouting a struct requires a full view of the node tree in
    // order to do type lookups, so these need to be constructed separately.
    // The node tree contains indices into this array.
    structs: []const Struct,

    pub fn compile(comptime contents: []const u8) !*const Schema {
        const ast = try parser.parseFile(contents);
        var num_structs: usize = 0;
        var schema: Schema = undefined;
        var tree = Node{.name=""};
        tree.nodes = try Node.compile(&ast.decls, &num_structs, &tree);
        schema.tree = &tree;

        var structs: [num_structs]Struct = undefined;
        schema.structs = &structs;
        num_structs = 0;
        try compileStructs(&ast.decls, &tree, &structs, &num_structs);
        for (structs) |*s| s.schema = &schema;

        return &schema;
    }

    fn debugWrite(comptime schema: *const Schema, wr: anytype) !void {
        try schema.tree.debugWrite(schema, wr, 0);
    }
};


fn compileStructs(comptime ast: *const parser.Decls, comptime loc: *const Node, comptime out: []Struct, out_n: *usize) !void {
    var structs = ast.structs;
    while (structs) |e| : (structs = e.next) {
        const sub = loc.get(e.name) orelse unreachable;
        out[out_n.*] = try Struct.compile(e, sub);
        out_n.* += 1;
        try compileStructs(&e.decls, sub, out, out_n);
    }
}


pub const Node = struct {
    name: []const u8,
    ntype: NType = .{ .root = {} },
    nodes: []const *const Node = &.{}, // sorted by name
    parent: ?*const Node = null,

    pub const NType = union(enum) {
        root: void,
        tstruct: usize, // Index into the schema's 'structs' array
        tenum: type,
    };

    fn lessThan(_: void, comptime a: *const Node, comptime b: *const Node) bool {
        return std.mem.lessThan(u8, a.name, b.name);
    }

    fn order(_: void, comptime a: *const Node, comptime b: *const Node) std.math.Order {
        return std.mem.order(u8, a.name, b.name);
    }

    fn ll_length(head: anytype) usize {
        var n: usize = 0;
        var lst = head;
        while (lst) |e| : (lst = e.next) n += 1;
        return n;
    }

    fn compileEnum(comptime ast: *const parser.Enum) !type {
        var num_values: usize = 0;
        var enumerant = ast.enumerants;
        while (enumerant) |e| : (enumerant = e.next) num_values += 1;

        var fields = [1]std.builtin.Type.EnumField{.{ .name = "", .value = 0 }} ** num_values;
        enumerant = ast.enumerants;
        while (enumerant) |e| : (enumerant = e.next) {
            if (e.ord >= num_values) return error.InvalidOrdinal;
            if (fields[e.ord].name.len > 0) return error.DuplicateOrdinal;
            fields[e.ord] = .{ .name = e.name, .value = e.ord };
        }

        return @Type(.{.Enum = .{
            .tag_type = u16,
            .fields = &fields,
            .decls = &.{},
            .is_exhaustive = true,
        }});
    }

    fn compile(comptime ast: *const parser.Decls, comptime num_structs: *usize, comptime parent: ?*const Node) ![]const *const Node {
        var num_nodes = ll_length(ast.structs) + ll_length(ast.enums);
        var nodes: [num_nodes]*const Node = undefined;

        var structs = ast.structs;
        while (structs) |e| : (structs = e.next) {
            const idx = num_structs.*;
            num_structs.* += 1;
            num_nodes -= 1;
            var n = Node{
                .name = e.name,
                .ntype = .{.tstruct = idx},
                .parent = parent,
            };
            n.nodes = try compile(&e.decls, num_structs, &n);
            nodes[num_nodes] = &n;
        }

        var enums = ast.enums;
        while (enums) |e| : (enums = e.next) {
            num_nodes -= 1;
            nodes[num_nodes] = &.{
                .name = e.name,
                .ntype = .{.tenum = try compileEnum(e)},
                .parent = parent,
            };
        }

        std.sort.sort(*const Node, &nodes, {}, Node.lessThan);

        for (nodes) |s,n|
            if (n > 0 and std.mem.eql(u8, s.name, nodes[n-1].name))
                return error.DuplicateName;
        return &nodes;
    }

    fn get(comptime node: *const Node, comptime name: []const u8) ?*const Node {
        const idx = std.sort.binarySearch(*const Node, &.{.name=name}, node.nodes, {}, order) orelse return null;
        return node.nodes[idx];
    }

    // Traverse the given node with the path given in suf. Returns the last
    // matching node and suf is updated to point to the last non-matching
    // suffix or null if the full path has been traversed.
    fn lookupExprSuffix(comptime node: *const Node, comptime suf: *?*const parser.ExprSuffix) *const Node {
        switch ((suf.* orelse return node).val) {
            .path => |p| {
                if (node.get(p)) |n| {
                    suf.* = suf.*.?.next;
                    return lookupExprSuffix(n, suf);
                } else return node;
            },
            else => return node,
        }
    }

    fn lookupSub(comptime node: *const Node, comptime path: []const []const u8) ?*const Node {
        if (path.len == 0) return null;
        const n = node.get(path[0]) orelse return null;
        if (path.len == 1) return n
        else return lookupSub(n, path[1..path.len]);
    }

    fn lookup(comptime node_: *const Node, comptime path: []const []const u8) ?*const Node {
        var node: ?*const Node = node_;
        while (node) |e| : (node = e.parent) if (lookupSub(e, path)) |n| return n;
        return null;
    }

    fn debugWriteSub(comptime node: *const Node, comptime schema: *const Schema, wr: anytype, lvl: u32) @TypeOf(wr).Error!void {
        for (node.nodes) |e| {
            std.debug.assert(e.parent == node);
            try e.debugWrite(schema, wr, lvl);
        }
    }

    fn debugWrite(comptime node: *const Node, comptime schema: *const Schema, wr: anytype, lvl: u32) @TypeOf(wr).Error!void {
        switch (node.ntype) {
            .root => try node.debugWriteSub(schema, wr, lvl),
            .tstruct => |idx| {
                const c = &schema.structs[idx];
                try wr.writeByteNTimes(' ', lvl*2);
                try wr.print("S{} {s} # {} bytes, {} ptrs", .{ idx, node.name, c.data_words*8, c.pointer_count });
                if (c.discriminant) |d|
                    try wr.print(", tag bits [{},{})", .{ c.discriminants[d]*16, (c.discriminants[d]+1)*16 });
                try wr.writeByte('\n');
                for (c.body) |e| try e.debugWrite(c, wr, lvl+1);
                try node.debugWriteSub(schema, wr, lvl+1);
            },
            .tenum => |e| {
                std.debug.assert(node.nodes.len == 0);
                try wr.writeByteNTimes(' ', lvl*2);
                try wr.print("E {s}\n", .{ node.name });
                for (@typeInfo(e).Enum.fields) |f| {
                    try wr.writeByteNTimes(' ', (lvl+1)*2);
                    try wr.print("{s} @{}\n", .{ f.name, f.value });
                }
            },
        }
    }
};


pub const Struct = struct {
    schema: *const Schema = undefined, // filled out in Schema.compile()
    body: []const Member,
    discriminant: ?u16 = null,

    offsets: []const u32 = &.{}, // ordinal -> field offset
    discriminants: []const u32 = &.{},
    data_words: u16 = 0,
    pointer_count: u16 = 0,

    // Field or Group. Too lazy to separate these out into different structs.
    // Group has body.len > 0, otherwise it's a field.
    pub const Member = struct {
        name: []const u8,
        union_tag: ?u16,
        ord: u16 = 0, // For groups: lowest ord of its members
        // Fields
        type: Type = .Void,
        default: ?parser.Expr = null,
        // Groups
        body: []const Member = &.{}, // Sorted by ordinal
        discriminant: ?u16 = null, // If this group has a union, index into the Struct's discriminants array

        fn lessThan(_: void, comptime a: Member, comptime b: Member) bool { return a.ord < b.ord; }

        fn debugWrite(comptime self: *const Member, comptime top: *const Struct, wr: anytype, lvl: u32) @TypeOf(wr).Error!void {
            try wr.writeByteNTimes(' ', lvl*2);
            if (self.name.len > 0) try wr.print("{s}", .{self.name});
            if (self.union_tag) |n| try wr.print(" *{}", .{n});
            if (self.body.len > 0) {
                if (self.discriminant) |d|
                    try wr.print(" # tag bits [{},{})", .{ top.discriminants[d]*16, (top.discriminants[d]+1)*16 });
                try wr.writeByte('\n');
                for (self.body) |e| try e.debugWrite(top, wr, lvl+1);
            } else {
                try wr.print(" @{} :", .{self.ord});
                try self.type.debugWrite(wr);
                if (self.type.dataSize()) |s|
                    try wr.print(" # bits[{},{})\n", .{ top.offsets[self.ord]<<s, (1+top.offsets[self.ord])<<s })
                else if (self.type == .Void)
                    try wr.writeAll("\n")
                else
                    try wr.print(" # ptr[{}]\n", .{top.offsets[self.ord]});
            }
        }
    };

    // Members are sorted by their ordinal
    fn compileBody(comptime ast: *const parser.StructBody, comptime loc: *const Node, comptime discriminant: *u16, comptime ngroups: *u16, comptime nfields: *u16, comptime maxord: *u16) ![]const Member {
        var nmembers: u32 = 0;
        var inunion: u32 = 0;
        var fields = ast.fields;
        while (fields) |e| : (fields = e.next) {
            if (e.in_union) inunion += 1
            else nmembers += 1;
        }
        var groups = ast.groups;
        while (groups) |e| : (groups = e.next) {
            if (e.in_union) inunion += 1
            else nmembers += 1;
        }
        if (inunion == 1) return error.NotEnoughMembersInUnion;

        var members: [nmembers + inunion]Member = undefined;
        nmembers = 0;

        fields = ast.fields;
        while (fields) |e| : (fields = e.next) {
            members[nmembers] = Member{
                .name = e.name,
                .union_tag = if (e.in_union) 0 else null,
                .ord = e.ord,
                .type = try Type.parse(loc, &e.type),
                .default = e.default,
            };
            nmembers += 1;
            nfields.* += 1;
            if (e.ord > maxord.*) maxord.* = e.ord;
        }

        groups = ast.groups;
        while (groups) |e| : (groups = e.next) {
            ngroups.* += 1;
            const body = try compileBody(&e.body, loc, discriminant, ngroups, nfields, maxord);
            if (body.len == 0) return error.EmptyGroup;
            const hasunion = for (body) |b| { if (b.union_tag != null) break true; } else false;
            members[nmembers] = Member{
                .name = e.name,
                .union_tag = if (e.in_union) 0 else null,
                .body = body,
                .ord = body[0].ord,
                .discriminant = if (hasunion) blk: { discriminant.* += 1; break :blk discriminant.* - 1; } else null,
            };
            nmembers += 1;
        }

        std.sort.sort(Member, &members, {}, Member.lessThan);
        var tag: u16 = 0;
        for (members) |*m| {
            if (m.union_tag) |*t| {
                t.* = tag;
                tag += 1;
            }
        }
        return &members;
    }

    fn compile(comptime ast: *const parser.Struct, comptime loc: *const Node) !Struct {
        var discriminant: u16 = 0;
        var ngroups: u16 = 1;
        var maxord: u16 = 0;
        var nfields: u16 = 0;
        const body = try compileBody(&ast.body, loc, &discriminant, &ngroups, &nfields, &maxord);
        const hasunion = for (body) |b| { if (b.union_tag != null) break true; } else false;
        const top_discriminant: ?u16 = if (hasunion) blk: {
            discriminant += 1;
            break :blk discriminant - 1;
        } else null;

        if (nfields > 0 and maxord != nfields-1) return error.InvalidOrdinal;

        // Do the layouting thing
        var groups: [ngroups]LayoutGroup = undefined;
        var offsets: [nfields]u32 = undefined;
        var discriminants: [discriminant]u32 = undefined;
        var nextgroup: u16 = 1;
        var nextord: u16 = 0;
        groups[0] = .{
            .members = body,
            .discriminant = if (top_discriminant) |i| &discriminants[i] else null
        };
        while (
            for (groups) |*g, n| {
                // This only happens if there's a gap in the given ordinals,
                // which can happen if the schema has duplicate ordinals. The
                // code above doesn't detect that case.
                if (n >= nextgroup) return error.InvalidOrdinal;
                if (g.idx < g.members.len and g.members[g.idx].ord == nextord) {
                    break @as(?*LayoutGroup, g);
                }
            } else null
        ) |g| {
            const m = g.members[g.idx];
            g.idx += 1;
            if (m.body.len > 0) {
                groups[nextgroup] = .{
                    .members = m.body,
                    .proxy = if (m.union_tag == null) g else null,
                    .parent = if (m.union_tag == null) null else g,
                    .discriminant = if (m.discriminant) |i| &discriminants[i] else null,
                };
                nextgroup += 1;
            } else {
                // Always add a member through a separate LayoutGroup. For
                // non-union members this doesn't matter as the call just
                // proxies upward, but for union members this keeps the
                // layouting algorithm in line with the C++ implementation.
                var memgroup = LayoutGroup{
                    .members = &.{},
                    .proxy = if (m.union_tag == null) g else null,
                    .parent = if (m.union_tag == null) null else g,
                };
                offsets[nextord] =
                    if (m.type.dataSize()) |s| memgroup.addData(s)
                    else if (m.type == .Void) blk: { memgroup.addVoid(); break :blk 0; }
                    else memgroup.addPointer();
                nextord += 1;
            }
        }
        if (nextord < maxord) return error.InvalidOrdinal; // XXX: why not '<='?
        return Struct{
            .body = body,
            .discriminant = top_discriminant,
            .offsets = &offsets,
            .discriminants = &discriminants,
            .data_words = groups[0].data_word_count,
            .pointer_count = groups[0].pointer_count,
        };
    }
};

const Lg = u3; // log of a data field's size, 0 representing a u1 and 6 a u64.

// A "Group" for the purposes of creating the struct layout. Each LayoutGroup
// corresponds to a '[]const Struct.Member' in the Struct tree and acts as an
// iterator through its members.
//
// In terms of the capnp C++ layout algorithm, each LayoutGroup represents both
// a (Top or Group or Proxy-for-a-TopOrGroup) and potentially a Union.
// Yes, this is rather messy and should perhaps be split up into multiple
// structs.
// Other than the extra messiness, the algorithm below is a very direct
// translation of the C++ code.
const LayoutGroup = struct {
    members: []const Struct.Member,
    idx: usize = 0,

    // If this is a proxy for a parent LayoutGroup. A "proxy" represents a
    // group of members that can be flattened into its parent, i.e. it's a
    // group that's not inside a union.
    proxy: ?*LayoutGroup = null,

    // If this is a Group inside a union.
    parent: ?*LayoutGroup = null,
    has_members: bool = false,
    pointer_location_usage: u32 = 0,
    data_location_usage: std.BoundedArray(DataLocationUsage, MAXDATA) = .{},

    // If parent and proxy are null, then this represents the Top struct.
    holeset: HoleSet = .{},
    data_word_count: u16 = 0,
    pointer_count: u16 = 0,

    // If this group contains a union
    discriminant: ?*u32 = null, // where to write the disciminant offset
    group_count: u32 = 0, // Number of direct members in this union
    data_locations: std.BoundedArray(DataLocation, MAXDATA) = .{},
    pointer_locations: std.BoundedArray(u32, MAXPTR) = .{},

    // Maximum number of data/pointer locations in a union. This limit sucks
    // balls, but we can't do dynamic memory allocation at comptime so we need
    // to pre-allocate the arrays. Big waste of memory, but it works. Mostly.
    const MAXDATA: usize = 32;
    const MAXPTR: usize = 32;

    const DataLocation = struct {
        size: Lg,
        offset: u32,

        fn tryExpandTo(loc: *DataLocation, comptime u: *LayoutGroup, new_size: Lg) bool {
            if (new_size <= loc.size) return true;
            if (u.tryExpandData(loc.size, loc.offset, new_size - loc.size)) {
                loc.offset >>= new_size - loc.size;
                loc.size = new_size;
                return true;
            }
            return false;
        }
    };

    const DataLocationUsage = struct {
        is_used: bool = false,
        size_used: Lg = undefined,
        holeset: HoleSet = .{},

        fn smallestHoleAtLeast(usage: *const DataLocationUsage, loc: *const DataLocation, size: Lg) ?Lg {
            if (!usage.is_used) return if (size <= loc.size) loc.size else null;
            if (size >= usage.size_used) return if (size < loc.size) size else null;
            if (usage.holeset.smallestAtLeast(size)) |r| return r;
            return if (usage.size_used < loc.size) usage.size_used else null;
        }

        // The C++ version has an unused 'group' argument.
        fn allocateFromHole(usage: *DataLocationUsage, loc: *DataLocation, size: Lg) u32 {
            var result: u32 = 0;
            if (!usage.is_used) {
                std.debug.assert(size <= loc.size);
                result = 0;
                usage.is_used = true;
                usage.size_used = size;
            } else if (size >= usage.size_used) {
                std.debug.assert(size <= loc.size);
                usage.holeset.addHolesAtEnd(usage.size_used, 1, size);
                usage.size_used = size + 1;
                result = 1;
            } else if (usage.holeset.tryAllocate(size)) |hole| {
                result = hole;
            } else {
                result = @as(u32, 1) << (usage.size_used - size);
                usage.holeset.addHolesAtEnd(size, result + 1, usage.size_used);
                usage.size_used += 1;
            }
            return result + (loc.offset << (loc.size - size));
        }

        fn tryAllocateByExpanding(usage: *DataLocationUsage, comptime u: *LayoutGroup, loc: *DataLocation, size: Lg) ?u32 {
            if (!usage.is_used) {
                if (loc.tryExpandTo(u, size)) {
                    usage.is_used = true;
                    usage.size_used = size;
                    return loc.offset << (loc.size - size);
                } else return null;
            } else {
                const new_size = std.math.max(usage.size_used, size) + 1;
                if (usage.tryExpandUsage(u, loc, new_size, true)) {
                    const result = usage.holeset.tryAllocate(size).?;
                    const location_offset = loc.offset << (loc.size - size);
                    return location_offset + result;
                } else return null;
            }
        }

        fn tryExpand(usage: *DataLocationUsage, comptime u: *LayoutGroup, loc: *DataLocation, old_size: Lg, old_offset: u32, factor: Lg) bool {
            if (old_offset == 0 and usage.size_used == old_size)
                return usage.tryExpandUsage(u, loc, old_size + factor, false)
            else
                return usage.holeset.tryExpand(old_size, old_offset, factor);
        }

        fn tryExpandUsage(usage: *DataLocationUsage, comptime u: *LayoutGroup, loc: *DataLocation, desired_usage: Lg, new_holes: bool) bool {
            if (desired_usage > loc.size) {
                if (!loc.tryExpandTo(u, desired_usage)) return false;
            }
            if (new_holes) usage.holeset.addHolesAtEnd(usage.size_used, 1, desired_usage);
            usage.size_used = desired_usage;
            return true;
        }
    };

    fn tryExpandData(comptime g: *LayoutGroup, old_size: Lg, old_offset: u32, factor: Lg) bool {
        if (g.proxy) |p| return p.tryExpandData(old_size, old_offset, factor);

        // "Top"
        const parent = g.parent orelse return g.holeset.tryExpand(old_size, old_offset, factor);

        // "Group"
        if (old_size + factor > 6 or (old_offset & ((@as(u32, 1) << factor) - 1)) != 0) return false;
        for (g.data_location_usage.slice()) |*usage, i| {
            const loc = &parent.data_locations.slice()[i];
            if (loc.size >= old_size and old_offset >> (loc.size - old_size) == loc.offset) {
                const local_old_offset = old_offset - (loc.offset << (loc.size - old_size));
                return usage.tryExpand(parent, loc, old_size, local_old_offset, factor);
            }
        }
        unreachable;
    }

    fn newGroupAddingFirstMember(comptime u: *LayoutGroup) void {
        u.group_count += 1;
        if (u.group_count == 2) {
            // addDiscriminant() inlined here
            if (u.discriminant) |ptr| {
                ptr.* = u.addData(4);
                u.discriminant = null;
            }
        }
    }

    fn addMember(comptime g: *LayoutGroup) void {
        if (g.parent) |p| {
            if (!g.has_members) {
                g.has_members = true;
                p.newGroupAddingFirstMember();
            }
        }
    }

    fn addVoid(comptime g: *LayoutGroup) void {
        if (g.proxy) |p| return p.addVoid();
        g.addMember();
        if (g.parent) |p| p.addVoid();
    }

    fn addDataToGroup(comptime g: *LayoutGroup, comptime u: *LayoutGroup, size: Lg) u32 {
        var best_size: Lg = std.math.maxInt(Lg);
        var best_location: ?u32 = null;

        for (u.data_locations.slice()) |*loc, i| {
            if (g.data_location_usage.slice().len == i)
                g.data_location_usage.append(.{}) catch unreachable;
            const usage = &g.data_location_usage.slice()[i];
            if (usage.smallestHoleAtLeast(loc, size)) |hole| {
                if (hole < best_size) {
                    best_size = hole;
                    best_location = @intCast(u32, i);
                }
            }
        }
        if (best_location) |best|
            return g.data_location_usage.slice()[best].allocateFromHole(&u.data_locations.slice()[best], size);

        for (u.data_locations.slice()) |*loc, i| {
            if (g.data_location_usage.slice()[i].tryAllocateByExpanding(u, loc, size)) |r|
                return r;
        }

        // addNewDataLocation() inlined here
        const off = u.addData(size);
        u.data_locations.append(.{ .size = size, .offset = off }) catch unreachable;
        g.data_location_usage.append(.{.is_used = true, .size_used = size}) catch unreachable;
        return off;
    }

    fn addData(comptime g: *LayoutGroup, size: Lg) u32 {
        if (g.proxy) |p| return p.addData(size);
        g.addMember();

        // "Group"
        if (g.parent) |p| return g.addDataToGroup(p, size);

        // "Top"
        if (g.holeset.tryAllocate(size)) |off| return off;
        const off = g.data_word_count << (6 - size);
        g.data_word_count += 1;
        g.holeset.addHolesAtEnd(size, off + 1, 6);
        return off;
    }

    fn addPointer(comptime g: *LayoutGroup) u32 {
        if (g.proxy) |p| return p.addPointer();
        g.addMember();

        // "Group"
        if (g.parent) |p| {
            g.pointer_location_usage += 1;
            if (g.pointer_location_usage > p.pointer_locations.slice().len)
                p.pointer_locations.append(p.addPointer()) catch unreachable;
            return p.pointer_locations.slice()[g.pointer_location_usage-1];
        }

        // "Top"
        defer g.pointer_count += 1;
        return g.pointer_count;
    }
};


const HoleSet = struct {
    holes: [6]u32 = [1]u32{0} ** 6,
    const Self = @This();

    fn tryAllocate(self: *Self, size: Lg) ?u32 {
        if (size >= self.holes.len) return null;
        if (self.holes[size] != 0) {
            defer self.holes[size] = 0;
            return self.holes[size];
        }
        if (self.tryAllocate(size + 1)) |r| {
            const result = r * 2;
            self.holes[size] = result + 1;
            return result;
        }
        return null;
    }

    fn addHolesAtEnd(self: *Self, size: Lg, offset: u32, limit: Lg) void {
        var n = size;
        var off = offset;
        while (n < limit) {
            std.debug.assert(self.holes[n] == 0);
            std.debug.assert(off % 2 == 1);
            self.holes[n] = off;
            n += 1;
            off = (off + 1) / 2;
        }
    }

    fn tryExpand(self: *Self, old_size: Lg, old_offset: u32, factor: Lg) bool {
        if (factor == 0) return true;
        if (old_size == self.holes.len) return false;
        if (self.holes[old_size] != old_offset + 1) return false;
        if (self.tryExpand(old_size + 1, old_offset >> 1, factor - 1)) {
            self.holes[old_size] = 0;
            return true;
        } else return false;
    }

    fn smallestAtLeast(self: *const Self, size: Lg) ?Lg {
        var n = size;
        while (n < self.holes.len) : (n += 1) {
            if (self.holes[n] != 0) return n;
        }
        return null;
    }
};


pub const Type = union(enum) {
    Void,
    Bool,
    Int8,
    Int16,
    Int32,
    Int64,
    UInt8,
    UInt16,
    UInt32,
    UInt64,
    Float32,
    Float64,
    Text,
    Data,
    AnyPointer,
    List: *const Type,
    Enum: type,
    Struct: usize, // TODO: struct parameters

    pub fn BaseType(comptime t: Type) type {
        return switch (t) {
            .Void => void,
            .Bool => bool,
            .Int8 => i8,
            .Int16 => i16,
            .Int32 => i32,
            .Int64 => i64,
            .UInt8 => u8,
            .UInt16 => u16,
            .UInt32 => u32,
            .UInt64 => u64,
            .Float32 => f32,
            .Float64 => f64,
            .Text => [:0]const u8,
            .Data => []const u8,
            else => unreachable,
        };
    }

    // Returns null for Void or pointer types
    fn dataSize(comptime t: Type) ?Lg {
        return switch (t) {
            .Void => null,
            .Bool => 0,
            .Int8 => 3,
            .Int16 => 4,
            .Int32 => 5,
            .Int64 => 6,
            .UInt8 => 3,
            .UInt16 => 4,
            .UInt32 => 5,
            .UInt64 => 6,
            .Float32 => 5,
            .Float64 => 6,
            .Text => null,
            .Data => null,
            .AnyPointer => null,
            .List => null,
            .Enum => 4,
            .Struct => null,
        };
    }

    fn parseBaseType(comptime t: []const u8) ?Type {
        const eql = std.mem.eql;
        if (eql(u8, t, "Void")) return .Void;
        if (eql(u8, t, "Bool")) return .Bool;
        if (eql(u8, t, "Int8")) return .Int8;
        if (eql(u8, t, "Int16")) return .Int16;
        if (eql(u8, t, "Int32")) return .Int32;
        if (eql(u8, t, "Int64")) return .Int64;
        if (eql(u8, t, "UInt8")) return .UInt8;
        if (eql(u8, t, "UInt16")) return .UInt16;
        if (eql(u8, t, "UInt32")) return .UInt32;
        if (eql(u8, t, "UInt64")) return .UInt64;
        if (eql(u8, t, "Float32")) return .Float32;
        if (eql(u8, t, "Float64")) return .Float64;
        if (eql(u8, t, "Text")) return .Text;
        if (eql(u8, t, "Data")) return .Data;
        if (eql(u8, t, "AnyPointer")) return .AnyPointer;
        return null;
    }

    fn parse(comptime loc: *const Node, comptime expr: *const parser.Expr) error{InvalidType}!Type {
        const node = switch (expr.base) {
            .identifier => |s| blk: {
                if (parseBaseType(s)) |t| {
                    if (expr.suffix != null) return error.InvalidType;
                    return t;
                }
                if (std.mem.eql(u8, s, "List")) {
                    if (expr.suffix == null or expr.suffix.?.next != null) return error.InvalidType;
                    switch (expr.suffix.?.val) {
                        .tuple => |t| {
                            if (t) |e| {
                                if (e.next != null or e.name != null) return error.InvalidType;
                                const el = try parse(loc, &e.expr);
                                return Type{.List = &el};
                            } else return error.InvalidType;
                        },
                        else => return error.InvalidType,
                    }
                }
                break :blk loc.lookup(&.{s}) orelse return error.InvalidType;
            },
            else => return error.InvalidType,
        };
        var suf = expr.suffix;
        const final = node.lookupExprSuffix(&suf);
        if (suf != null) return error.InvalidType; // TODO: Parameter list is valid for structs
        return switch (final.ntype) {
            .root => unreachable,
            .tstruct => |idx| Type{.Struct = idx},
            .tenum => |e| Type{.Enum = e},
        };
    }

    fn debugWrite(comptime t: Type, wr: anytype) @TypeOf(wr).Error!void {
        try wr.writeAll(@tagName(std.meta.activeTag(t)));
        switch (t) {
            .List => |e| {
                try wr.writeByte('(');
                try e.debugWrite(wr);
                try wr.writeByte(')');
            },
            .Struct => |idx| try wr.print(".{}", .{ idx }),
            else => {},
        }
    }
};

fn testSchema(comptime input: []const u8, comptime output: []const u8) !void {
    const buf = comptime blk: {
        @setEvalBranchQuota(500000);
        const schema = Schema.compile(input) catch unreachable;
        var buf: [2046]u8 = undefined;
        var wr = std.io.fixedBufferStream(&buf);
        try schema.debugWrite(wr.writer());
        break :blk wr.getWritten();
    };
    try std.testing.expectEqualStrings(output, buf);
}

test "Flat data" {
    try testSchema(
    \\@0x89cc87421cfbe567;
    \\struct X {
    \\  f0 @0 :Bool;
    \\  f1 @1 :Int16;
    \\  f2 @2 :Int8;
    \\  f3 @3 :Int64;
    \\  f4 @4 :Bool;
    \\  f5 @5 :UInt16;
    \\  f6 @6 :Int8;
    \\}
    \\
    ,
    \\S0 X # 16 bytes, 0 ptrs
    \\  f0 @0 :Bool # bits[0,1)
    \\  f1 @1 :Int16 # bits[16,32)
    \\  f2 @2 :Int8 # bits[8,16)
    \\  f3 @3 :Int64 # bits[64,128)
    \\  f4 @4 :Bool # bits[1,2)
    \\  f5 @5 :UInt16 # bits[32,48)
    \\  f6 @6 :Int8 # bits[48,56)
    \\
    );
}

test "Unions in groups" {
    try testSchema(
    \\@0x89cc87421cfbe567;
    \\struct Z {
    \\  grp :group {
    \\    union {
    \\      f0 @0 :Int8;
    \\      f1 @1 :Int16;
    \\    }
    \\  }
    \\  grp2 :union {
    \\    f2 @2 :Int8;
    \\    f3 @3 :Int16;
    \\  }
    \\  union {
    \\    f4 @4 :Int8;
    \\    f5 @5 :Int16;
    \\  }
    \\}
    \\
    ,
    \\S0 Z # 16 bytes, 0 ptrs, tag bits [80,96)
    \\  grp # tag bits [16,32)
    \\    f0 *0 @0 :Int8 # bits[0,8)
    \\    f1 *1 @1 :Int16 # bits[0,16)
    \\  grp2 # tag bits [48,64)
    \\    f2 *0 @2 :Int8 # bits[32,40)
    \\    f3 *1 @3 :Int16 # bits[32,48)
    \\  f4 *0 @4 :Int8 # bits[64,72)
    \\  f5 *1 @5 :Int16 # bits[64,80)
    \\
    );
}

test "Unions in groups, interleaved" {
    try testSchema(
    \\@0x89cc87421cfbe567;
    \\struct Z {
    \\  grp :group {
    \\    union {
    \\      f0 @0 :Int8;
    \\      f1 @3 :Int16;
    \\    }
    \\  }
    \\  grp2 :union {
    \\    f2 @1 :Int8;
    \\    f3 @4 :Int16;
    \\  }
    \\  union {
    \\    f4 @2 :Int8;
    \\    f5 @5 :Int16;
    \\  }
    \\}
    \\
    ,
    \\S0 Z # 16 bytes, 0 ptrs, tag bits [96,112)
    \\  grp # tag bits [32,48)
    \\    f0 *0 @0 :Int8 # bits[0,8)
    \\    f1 *1 @3 :Int16 # bits[48,64)
    \\  grp2 # tag bits [64,80)
    \\    f2 *0 @1 :Int8 # bits[8,16)
    \\    f3 *1 @4 :Int16 # bits[80,96)
    \\  f4 *0 @2 :Int8 # bits[16,24)
    \\  f5 *1 @5 :Int16 # bits[16,32)
    \\
    );
}

test "Groups in unions" {
    try testSchema(
    \\@0x89cc87421cfbe567;
    \\struct X {
    \\  union {
    \\    f0 @0 :Bool;
    \\    g0 :group {
    \\      f1 @1 :Int8;
    \\      f2 @2 :Void;
    \\    }
    \\    f3 @3 :Text;
    \\    g2 :group {
    \\      f4 @4 :Void;
    \\      f5 @5 :List(Int8);
    \\      f6 @6 :Int16;
    \\      f7 @7 :UInt64;
    \\      f8 @8 :Bool;
    \\    }
    \\  }
    \\}
    \\
    ,
    \\S0 X # 16 bytes, 1 ptrs, tag bits [16,32)
    \\  f0 *0 @0 :Bool # bits[0,1)
    \\  g0 *1
    \\    f1 @1 :Int8 # bits[0,8)
    \\    f2 @2 :Void
    \\  f3 *2 @3 :Text # ptr[0]
    \\  g2 *3
    \\    f4 @4 :Void
    \\    f5 @5 :List(Int8) # ptr[0]
    \\    f6 @6 :Int16 # bits[0,16)
    \\    f7 @7 :UInt64 # bits[64,128)
    \\    f8 @8 :Bool # bits[32,33)
    \\
    );
}

// From https://github.com/capnproto/capnproto/issues/344
// Run capnp with CAPNP_IGNORE_ISSUE_344=1 to get proper output.
test "Issue 344" {
    try testSchema(
    \\@0x89cc87421cfbe567;
    \\struct Fields {
    \\    field1 @0 :Int32;
    \\    field2 @1 :Int64;
    \\    field3 @2 :Int32;
    \\    eventType : union {
    \\        unknownEvent @3: Void;
    \\        sampleEvent : group {
    \\            field4 @4 :Int32;
    \\            request : union {
    \\                field5 @5: Void;
    \\                field6 : group {
    \\                    field7 @6 :Int16;
    \\                    field8 @7 :Int32;
    \\                    field9 @8 :Float64;
    \\                    field10 @9 :Int32;
    \\                }
    \\            }
    \\        }
    \\    }
    \\}
    ,
    \\S0 Fields # 48 bytes, 0 ptrs
    \\  field1 @0 :Int32 # bits[0,32)
    \\  field2 @1 :Int64 # bits[64,128)
    \\  field3 @2 :Int32 # bits[32,64)
    \\  eventType # tag bits [128,144)
    \\    unknownEvent *0 @3 :Void
    \\    sampleEvent *1
    \\      field4 @4 :Int32 # bits[160,192)
    \\      request # tag bits [144,160)
    \\        field5 *0 @5 :Void
    \\        field6 *1
    \\          field7 @6 :Int16 # bits[192,208)
    \\          field8 @7 :Int32 # bits[224,256)
    \\          field9 @8 :Float64 # bits[256,320)
    \\          field10 @9 :Int32 # bits[320,352)
    \\
    );
}

test "Node tree and type lookups" {
    try testSchema(
    \\@0x89cc87421cfbe567;
    \\struct Something {}
    \\struct X {
    \\  enum A { b@1; a@0; c@2; }
    \\  struct Y {
    \\      struct Z {
    \\          f0 @0 :Z;
    \\          f1 @1 :Y;
    \\          f2 @2 :X;
    \\          f3 @3 :X.A;
    \\          f4 @4 :Y.Z;
    \\          f5 @5 :X.Y.Z;
    \\          f6 @6 :Something;
    \\          f7 @7 :X.Something;
    \\      }
    \\  }
    \\  enum SomeEnum {}
    \\
    \\  # shadows top-level 'Something'. Not sure if it's possible to refer to
    \\  # the top-level one from this scope.
    \\  struct Something {}
    \\
    \\  a @0 :UInt16;
    \\  grp :group {
    \\    e1 @1 :UInt8;
    \\    e2 @2 :X;
    \\    e3 @3 :X.A;
    \\    e4 @4 :A;
    \\    e5 @5 :List(List(Y.Z));
    \\  }
    \\}
    ,
    \\S4 Something # 0 bytes, 0 ptrs
    \\S0 X # 8 bytes, 2 ptrs
    \\  a @0 :UInt16 # bits[0,16)
    \\  grp
    \\    e1 @1 :UInt8 # bits[16,24)
    \\    e2 @2 :Struct.0 # ptr[0]
    \\    e3 @3 :Enum # bits[32,48)
    \\    e4 @4 :Enum # bits[48,64)
    \\    e5 @5 :List(List(Struct.3)) # ptr[1]
    \\  E A
    \\    a @0
    \\    b @1
    \\    c @2
    \\  E SomeEnum
    \\  S1 Something # 0 bytes, 0 ptrs
    \\  S2 Y # 0 bytes, 0 ptrs
    \\    S3 Z # 8 bytes, 7 ptrs
    \\      f0 @0 :Struct.3 # ptr[0]
    \\      f1 @1 :Struct.2 # ptr[1]
    \\      f2 @2 :Struct.0 # ptr[2]
    \\      f3 @3 :Enum # bits[0,16)
    \\      f4 @4 :Struct.3 # ptr[3]
    \\      f5 @5 :Struct.3 # ptr[4]
    \\      f6 @6 :Struct.1 # ptr[5]
    \\      f7 @7 :Struct.1 # ptr[6]
    \\
    );
}
