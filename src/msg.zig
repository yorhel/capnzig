// SPDX-FileCopyrightText: 2022-2023 Yoran Heling <projects@yorhel.nl>
// SPDX-License-Identifier: MIT

const std = @import("std");
const schema = @import("schema.zig");

// This implementation uses std.mem.{read,write}IntLittle() to access encoded
// data. These functions support unaligned access and therefore may have a
// performance cost if the data is aligned. Need benchmarks to see if it's
// worth adding support for known-aligned buffers. Relevant discussion on the
// Rust implementation:
// - https://dwrensha.github.io/capnproto-rust/2020/01/11/unaligned-memory-access.html
// - https://dwrensha.github.io/capnproto-rust/2020/01/19/new-feature-to-allow-unaligned-buffers.html

const Pointer = struct {
    // Both the C++ and Rust implementations treat a pointer as two
    // little-endian u32's, rather than a single u64 or "bit string" as the
    // docs might suggest.
    //
    // This API may do more copying than is preferrable, not sure if/how that
    // affects performance; need benchmarks.

    offset_and_kind: u32,
    upper32bits: u32,

    const Kind = enum(u2) {
        Struct = 0,
        List = 1,
        Far = 2,
        Other = 3,
    };

    fn readBytes(bytes: *const [8]u8) Pointer {
        return .{
            .offset_and_kind = std.mem.readIntLittle(u32, bytes[0..4]),
            .upper32bits = std.mem.readIntLittle(u32, bytes[4..8]),
        };
    }

    fn writeBytes(ptr: Pointer, bytes: *[8]u8) void {
        std.mem.writeIntLittle(u32, bytes[0..4], ptr.offset_and_kind);
        std.mem.writeIntLittle(u32, bytes[4..8], ptr.upper32bits);
    }

    fn kind(ptr: Pointer) Kind {
        return @intToEnum(Kind, ptr.offset_and_kind & 3);
    }

    // Get the relative offset, only valid for struct and list pointers.
    fn positional(ptr: Pointer) i30 {
        return @bitCast(i30, @as(u30, ptr.offset_and_kind)>>2);
    }

    fn initStruct(offset: i30, data_words: u16, pointers: u16) Pointer {
        return .{
            .offset_and_kind = @bitCast(u32, @as(i32, offset))<<2,
            .upper32bits = @as(u32, data_words) | (@as(u32, pointers) << 16),
        };
    }
};

pub const Message = union(enum) {
    single_segment: std.ArrayList(u8), // First 8 bytes are used for the stream framing
    multi_segments: struct {
        segments: std.ArrayList(Segment),
        min_segment_size: u32 = 256,
    },
    const_segments: struct {
        segments: []const []const u8,
        allocator: ?std.mem.Allocator, // When the segments array should be freed on deinit()
    },

    const Segment = struct {
        buf: []u8,
        used: u32,
    };

    const Self = @This();

    /// Create a Message with a single segment.
    pub fn initSingleSegment(alloc: std.mem.Allocator) !Self {
        var buf = std.ArrayList(u8).init(alloc);
        try buf.resize(8);
        return Self{.single_segment = buf};
    }

    /// Write a new single-segment Message to a fixed buffer.
    pub fn initFixedBuffer(buf: []u8) !Self {
        // An Allocator that always fails, from std/mem.zig, where it's not marked 'pub' for some reason.
        const func = struct {
            fn failAllocatorAlloc(_: *anyopaque, n: usize, log2_alignment: u8, ra: usize) ?[*]u8 {
                _ = n;
                _ = log2_alignment;
                _ = ra;
                return null;
            }
        };
        const fail_allocator = std.mem.Allocator{
            .ptr = undefined,
            .vtable = &.{
                .alloc = func.failAllocatorAlloc,
                .resize = std.mem.Allocator.noResize,
                .free = std.mem.Allocator.noFree,
            }
        };
        var arraylist = std.ArrayList(u8).fromOwnedSlice(fail_allocator, buf);
        arraylist.clearRetainingCapacity();
        try arraylist.resize(8);
        return Self{.single_segment = arraylist};
    }

    /// Create a new message that may occupy multiple segments.
    pub fn initMultiSegment(alloc: std.mem.Allocator) Self {
        return .{.multi_segments = .{
            .segments = std.ArrayList(Segment).init(alloc),
        }};
    }

    /// Read a message from a buffer. The given buffer must remain valid for as
    /// long as the Message struct is in use.
    pub fn readMessage(alloc: std.mem.Allocator, msg: []const u8) !Self {
        // Need to be careful with overflow on 32bit systems, so don't use
        // regular + and * operators.
        const add = std.math.add;
        const mul = std.math.mul;

        if (msg.len < 8) return error.ShortMessage;
        const num_segments = try add(usize, 1, std.mem.readIntLittle(u32, msg[0..4]));

        var offset = try add(usize,
            if (num_segments & 1 == 1) @as(usize, 4) else 8,
            try mul(usize, 4, num_segments)
        );

        var seg = try alloc.alloc([]const u8, num_segments);
        for (seg) |*s, n| {
            // can't overflow since it's smaller than 'offset'
            const words = std.mem.readIntLittle(u32, msg[n*4+4..][0..4]);

            const next_offset = try add(usize, offset, try mul(usize, words, 8));
            if (msg.len < next_offset) return error.ShortMessage;
            s.* = msg[offset..next_offset];
            offset = next_offset;
        }
        return Self{.const_segments = .{
            .segments = seg,
            .allocator = alloc,
        }};
    }

    pub fn deinit(self: *Self) void {
        switch (self.*) {
            .single_segment => |s| s.deinit(),
            .multi_segments => |m| {
                for (m.segments.items) |s| m.segments.allocator.free(s.buf);
                m.segments.deinit();
            },
            .const_segments => |c| {
                if (c.allocator) |a| a.free(c.segments);
            },
        }
    }

    fn readOnly(self: *const Self) bool {
        return self.* == .const_segments;
    }

    fn segments(self: *const Self) u32 {
        return switch (self.*) {
            .single_segment => 1,
            .multi_segments => |m| @intCast(u32, m.segments.items.len),
            .const_segments => |c| @intCast(u32, c.segments.len),
        };
    }

    fn constSegment(self: *const Self, segment: u32) []const u8 {
        std.debug.assert(segment < self.segments());
        return switch (self.*) {
            .single_segment => |s| s.items[8..],
            .multi_segments => |m| m.segments.items[segment].buf,
            .const_segments => |c| c.segments[segment],
        };
    }

    fn mutSegment(self: *const Self, segment: u32) []u8 {
        std.debug.assert(segment < self.segments());
        return switch (self.*) {
            .single_segment => |s| s.items[8..],
            .multi_segments => |m| m.segments.items[segment].buf,
            .const_segments => unreachable,
        };
    }

    const Allocation = struct { segment: u32, offset: u32 };

    // XXX: This API is kinda bad, since the number of pointers that need
    // to be written is dependent on the segment in which an object is
    // allocated. So the API should support something along the lines of
    // "allocate this much space in this segment, or allocate this + 8 bytes in
    // another segment".
    fn allocate(self: *Self, size: u32) !Allocation {
        switch (self.*) {
            .single_segment => |*s| {
                const ret = Allocation{.segment = 0, .offset = @intCast(u32, s.items.len)-8};
                const newlen = try std.math.add(u32, @intCast(u32, s.items.len), size);
                try s.resize(newlen);
                return ret;
            },
            .multi_segments => |*m| {
                // Only attempt to allocate to the last segment, assume previous segments have already been filled.
                // This may waste some space, but should be much faster than iterating over all segments.
                if (m.segments.items.len > 0) {
                    const s = &m.segments.items[m.segments.items.len-1];
                    const ret = Allocation{
                        .segment = @intCast(u32, m.segments.items.len) - 1,
                        .offset = s.used
                    };
                    const free = @intCast(u32, s.buf.len) - s.used;
                    if (free >= size) {
                        s.used += size;
                        return ret;
                    }
                    // Attempt in-place growing
                    if (std.math.add(u32, @intCast(u32, s.buf.len), size - free)) |newlen| {
                        if (m.segments.allocator.resize(s.buf, newlen)) {
                            s.buf.len = newlen;
                            s.used += size;
                            return ret;
                        }
                    } else |_| {}
                }
                // Allocate a new segment
                const len = std.math.max(size, m.min_segment_size);
                const ret = Allocation{.segment = @intCast(u32, m.segments.items.len), .offset = 0};
                try m.segments.append(.{
                    .buf = try m.segments.allocator.alloc(u8, len),
                    .used = size,
                });
                return ret;
            },
            .const_segments => unreachable,
        }
    }

    fn writeDataField(self: *Self, segment: u32, data_offset: u32, field_offset: u32, comptime T: type, value: T) void {
        const buf = self.mutSegment(segment)[data_offset..];
        const wr = std.mem.writeIntLittle;
        switch (T) {
            bool => {
                if (value) buf[field_offset/8] |= @as(u8,1)<<(field_offset%8)
                else buf[field_offset] &= (0xff ^ @as(u8,1)<<(field_offset%8));
            },
            u8 => buf[field_offset] = value,
            i8 => buf[field_offset] = @bitCast(u8, value),
            u16 => wr(u16, buf[field_offset*2..][0..2], value),
            i16 => wr(i16, buf[field_offset*2..][0..2], value),
            u32 => wr(u32, buf[field_offset*4..][0..4], value),
            i32 => wr(i32, buf[field_offset*4..][0..4], value),
            f32 => wr(u32, buf[field_offset*4..][0..4], @bitCast(u32, value)),
            u64 => wr(u64, buf[field_offset*8..][0..8], value),
            i64 => wr(i64, buf[field_offset*8..][0..8], value),
            f64 => wr(u64, buf[field_offset*8..][0..8], @bitCast(u64, value)),
            else => unreachable,
        }
    }

    pub fn root(self: *Self, comptime T: type) !T {
        if (self.segments() == 0 or self.constSegment(0).len == 0) {
            if (self.readOnly()) return error.EmptyMessage;
            // Allocate the pointer and struct in a single block
            const loc = try self.allocate(8 + (T.schema_pointer_count + T.schema_data_words)*8);
            std.debug.assert(loc.segment == 0 and loc.offset == 0);
            Pointer.initStruct(0, T.schema_pointer_count, T.schema_data_words).writeBytes(self.mutSegment(0)[0..8]);
            std.mem.set(u8, self.mutSegment(0)[8..(1 + T.schema_pointer_count + T.schema_data_words)*8], 0);
            return T{
                .message = self,
                .segment = 0,
                .offset = 8,
                .data_size = T.schema_data_words,
                .pointers = T.schema_pointer_count,
            };
        }
        // TODO: Read root pointer
        unreachable;
    }

    // TODO: output methods
};


pub fn Group(struct_: *const schema.Struct, members: []const schema.Member, discriminant: ?u16) type {
    _ = discriminant;
    return struct {
        top: *const Struct(struct_),

        const Self = @This();

        pub const FieldEnum = blk: {
            var fields: [members.len]std.builtin.Type.EnumField = undefined;
            for (members) |*m, n| fields[n] = .{ .name = m.name, .value = n };
            break :blk @Type(.{.Enum = .{
                .layout = .Auto,
                .tag_type = u16,
                .fields = &fields,
                .decls = &.{},
                .is_exhaustive = true,
            }});
        };

        /// Type of fields supported by get() and set().
        /// (Also works for group and struct types, despite not being supported
        /// by get() or set())
        pub fn FieldType(field: FieldEnum) type {
            const m = &members[@enumToInt(field)];
            if (m.body > 0) return Group(struct_, m.body, m.discriminant);
            // TODO: List(non-pointer-type)
            switch (m.type) {
                .Ref => |r| {
                    if (r.tbl == .enums) return struct_.schema.enums[r.idx].Type();
                    if (r.tbl == .structs) return Struct(struct_.schema.schemas[r.idx]);
                },
                else => return m.type.BaseType(),
            }
            unreachable;
        }

        pub fn set(self: Self, comptime field: FieldEnum, value: FieldType(field)) void {
            std.debug.assert(!self.top.message.readOnly());
            const m = &members[@enumToInt(field)];
            // TODO: Set union discriminant

            if (m.type.dataSize() != null)
                self.top.message.writeDataField(self.top.segment, self.top.offset, struct_.offsets[m.ord], FieldType(field), value);
            if (m.type == .Void) return;
            // TODO: Support Text, Data and List(non-pointer-type)
        }
    };
}

pub fn Struct(comptime t: *const schema.Struct) type {
    return struct {
        message: *Message,
        segment: u32, // index
        offset: u32, // bytes
        data_size: u16, // words
        pointers: u16,

        pub const schema_pointer_count = t.pointer_count;
        pub const schema_data_words = t.data_words;
    };
}

test "refs" {
    std.testing.refAllDecls(Message);
}

test "Simple write" {
    const testschema = comptime schema.Schema.compile(
    \\@0x89cc87421cfbe567;
    \\struct S {
    \\  f @0 :Bool;
    \\}
    ) catch unreachable;

    var buf: [1024]u8 = undefined;
    var alloc = std.heap.FixedBufferAllocator.init(&buf);
    var msg = try Message.initSingleSegment(alloc.allocator());
    _ = try msg.root(Struct(&testschema.structs[0]));
}
