// SPDX-FileCopyrightText: 2022-2023 Yoran Heling <projects@yorhel.nl>
// SPDX-License-Identifier: MIT

test "imports" {
    _ = @import("parser.zig");
    _ = @import("msg.zig");
    _ = @import("schema.zig");
}
