// SPDX-FileCopyrightText: 2022-2023 Yoran Heling <projects@yorhel.nl>
// SPDX-License-Identifier: MIT
//
// As far as I'm aware, there's no exact specification of the capnproto schema
// language, so I had to improvise a bit; scanning through the C++ source and
// trying out some things with the capnp tool. There's bound to be some
// differences in how schema files are parsed and interpreted.
// Known differences:
// - capnp allows a space between the sign and the number; e.g. '- 1' is -1
// - This parser doesn't handle negative hex literals (e.g. '-0xFF')
// - This parser doesn't accept 'inf' or '-inf' float literals
//   (the former would be ambigious with identifiers, ouch)
// - This parser allows whitespace between nibbles in hex strings
//   (e.g. 0x"a b"), which capnp disallows.
// - Out-of-bounds octal escapes in strings seem to be handled differently,
//   this parser interprets "\777" as "\x3f7", capnp saturates to "0xff"?
//
// This parser is intended to be small and simple and primarily to be used for
// comptime loading of schema files. This comes with some limitations:
// - Error reporting is complete garbage.
// - This parser may be more lax about errors than the C++ implementation, the
//   assumption is that the given schema files have already been validated with
//   the capnp tool.
// - Only the Lexer can be used at run-time, the parser currently only works
//   with comptime-known strings. This can be fixed if/when mem.Allocator is
//   available at comptime.
//   https://github.com/ziglang/zig/issues/1291
//
// To the best of my knowledge, the order in which declarations appear in the
// file has no semantic meaning, so this parser makes no attempt to preserve
// declaration order in the AST.
//
// TODO: Figure out a way to resolve import/embed expressions.

const std = @import("std");
const Lexer = @import("Lexer.zig");
const Token = Lexer.Token;

// Generic declarations available at file scope and inside structs and interfaces
pub const Decls = struct {
    structs: ?*const Struct = null,
    enums: ?*const Enum = null,
    // TODO: interface, annotation, const, using
};

pub const File = struct {
    decls: Decls = .{},
    file_id: u64 = 0,
};

pub const Struct = struct {
    next: ?*const Struct = null,
    name: []const u8,
    uid: u64 = 0,
    decls: Decls = .{},
    body: StructBody = .{},
    // TODO: parameters
};

pub const StructBody = struct {
    fields: ?*const Field = null,
    groups: ?*const Group = null,
};

pub const Field = struct {
    next: ?*const Field = null,
    name: []const u8,
    ord: u16,
    in_union: bool,
    type: Expr,
    default: ?Expr = null,
};

pub const Group = struct {
    next: ?*const Group = null,
    name: []const u8,
    in_union: bool,
    body: StructBody = .{},
};

pub const Enum = struct {
    next: ?*const Enum = null,
    name: []const u8,
    uid: u64 = 0,
    enumerants: ?*const Enumerant = null,
};

pub const Enumerant = struct {
    next: ?*const Enumerant = null,
    name: []const u8,
    ord: u16,
};

pub const Expr = struct {
    base: ExprBase,
    suffix: ?*const ExprSuffix,
};

pub const ExprBase = union(enum) {
    integer: i65,
    number: f64,
    string: []const u8,
    binary: []const u8,
    brackets: ?*const ExprListItem, // bracketed list never has named tuple elements
    parens: ?*const ExprListItem,
    root_identifier: []const u8,
    identifier: []const u8,
    // TODO: import, embed
};

pub const ExprListItem = struct {
    next: ?*const ExprListItem = null,
    name: ?[]const u8 = null,
    expr: Expr,
};

pub const ExprSuffix = struct {
    next: ?*const ExprSuffix = null,
    val: ExprSuffixVal,
};

pub const ExprSuffixVal = union(enum) {
    path: []const u8,
    tuple: ?*const ExprListItem,
};

const Error = Lexer.Error || error{
    UnexpectedEof,
    MultipleFileIds,
    InvalidUid,
    InvalidOrdinal,
    InvalidHex,
    InvalidFieldName,
    InvalidTypeName,
    InvalidNestedUnion,
    Syntax
};

// Parses the token after '@'
fn expectUid(t: Token) !u64 {
    return switch (t) {
        .integer => |n| if (n < (1<<63)) error.InvalidUid else @intCast(u64, n),
        else => error.Syntax,
    };
}

// Parses the token after '@'
fn expectOrdinal(t: Token) !u16 {
    return switch (t) {
        .integer => |n| if (n < 0 or n > 65535) error.InvalidOrdinal else @intCast(u16, n),
        else => error.Syntax,
    };
}

fn expectTypeName(t: Token) ![]const u8 {
    return switch (t) {
        .identifier => |s| if (!std.ascii.isUpper(s[0])) error.InvalidTypeName else s,
        else => error.Syntax,
    };
}

fn expectFieldName(t: Token) ![]const u8 {
    return switch (t) {
        .identifier => |s| if (!std.ascii.isLower(s[0])) error.InvalidFieldName else s,
        else => error.Syntax,
    };
}

fn hexdigit(c: u8) ?u8 {
    return switch (c) {
        '0'...'9' => c - '0',
        'a'...'f' => c - 'a' + 10,
        'A'...'F' => c - 'A' + 10,
        else => null,
    };
}

fn hex2bin(comptime hex: []const u8) ![]const u8 {
    var n: usize = 0;
    for (hex) |c| {
        if (std.ascii.isHex(c)) n += 1;
    }
    if (n & 1 == 1) return error.InvalidHex;
    var out: [n>>1]u8 = undefined;
    n = 0;
    for (hex) |c| {
        const v = hexdigit(c) orelse continue;
        if (n & 1 == 0) out[n>>1] = v<<4
        else out[n>>1] |= v;
        n += 1;
    }
    return &out;
}

// Parse a comma-separated list of (possibly named) Exprs, starting after
// '[' or '('. close indicates the closing character.  Supports empty lists
// and a trailing comma.
fn parseExprList(comptime lex: *Lexer, close: u8) !?*const ExprListItem {
    var lst: ?*ExprListItem = null;
    var lst_end: ?*ExprListItem = null;
    var t = try lex.take();
    while (true) {
        if (t.eql(.{.special = close})) return lst;

        var exp = ExprListItem{.expr = try parseExpr(lex, &t)};
        if (lst == null) lst = &exp;
        if (lst_end) |e| e.next = &exp;
        lst_end = &exp;

        if (t.eql(.{.special = '='})) { // named tuple
            if (exp.expr.suffix != null) return error.Syntax;
            switch (exp.expr.base) {
                .identifier => |s| {
                    exp.name = s;
                    t = try lex.take();
                    exp.expr = try parseExpr(lex, &t);
                },
                else => return error.Syntax,
            }
        }

        if (t.eql(.{.special = ','})) t = try lex.take()
        else if (t.eql(.{.special = close})) return lst
        else return error.Syntax;
    }
}

// Parse a sequence of string tokens into a single unescaped string.
// Assumes the strings have already been validated by the lexer.
fn parseString(comptime lex: *Lexer, token: *Token) ![:0]const u8 {
    const L = struct { s: []const u8, next: ?*@This() = null };
    var lst: ?*L = null;
    var lst_end: ?*L = null;
    var bufsize: usize = 1;
    while (true) {
        switch (token.*) {
            .string => |s| {
                bufsize += s.len;
                var l = L{.s = s};
                if (lst == null) lst = &l;
                if (lst_end) |e| e.next = &l;
                lst_end = &l;
                token.* = try lex.take();
            },
            else => break,
        }
    }
    var buf: [bufsize]u8 = undefined;
    var i: usize = 0;
    while (lst) |e| : (lst = e.next) {
        const s = e.s;
        var n: usize = 0;
        while (n < s.len) {
            if (s[n] != '\\') {
                buf[i] = s[n];
                i += 1;
                n += 1;
                continue;
            }
            n += 1;
            if (s[n] == 'x') {
                buf[i] = ((hexdigit(s[n+1]) orelse unreachable) << 4)
                       | (hexdigit(s[n+2]) orelse unreachable);
                i += 1;
                n += 3;
                continue;
            }
            if (s[n] >= '0' and s[n] <= '7') {
                buf[i] = s[n] - '0';
                n += 1;
                if (n < s.len and s[n] >= '0' and s[n] <= '7') {
                    buf[i] = (buf[i] << 3) | (s[n] - '0');
                    n += 1;
                }
                if (n < s.len and s[n] >= '0' and s[n] <= '7' and buf[i] <= 0b0001_1111) {
                    buf[i] = (buf[i] << 3) | (s[n] - '0');
                    n += 1;
                }
                i += 1;
                continue;
            }
            buf[i] = switch (s[n]) {
                'a' => 0x07,
                'b' => 0x08,
                'f' => 0x0C,
                'n' => '\n',
                'r' => '\r',
                'v' => 0x0B,
                else => s[n],
            };
            i += 1;
            n += 1;
        }
    }
    buf[i] = 0;
    return buf[0..i:0];
}


// Parses an ExprBase, starting with the given token and 'token' is updated to
// point to the token after the base.
// The returned Expr will only have a suffix if the expression is surrounded by
// parens, e.g. '(Path.Name)'.
fn parseExprBase(lex: *Lexer, comptime token: *Token) Error!Expr {
    var consumed_next = false;
    const base = switch (token.*) {
        .integer => |i| ExprBase{.integer = i},
        .number => |f| ExprBase{.number = f},
        .string => blk: {
            consumed_next = true;
            break :blk ExprBase{.string = try parseString(lex, token)};
        },
        .hex_string => |s| ExprBase{.binary = try hex2bin(s)},
        .identifier => |s| ExprBase{.identifier = s},
        else => blk: {
            if (token.eql(.{.special = '.'})) {
                switch (try lex.take()) {
                    .identifier => |s| break :blk ExprBase{.root_identifier = s},
                    else => return error.Syntax,
                }

            } else if(token.eql(.{.special = '('})) {
                var lst = try parseExprList(lex, ')');
                // Single-element unnamed paren lists are equivalent to the embedded expression
                if (lst) |e| {
                    if (e.next == null and e.name == null) {
                        token.* = try lex.take();
                        return e.expr;
                    }
                }
                break :blk ExprBase{.parens = lst};

            } else if(token.eql(.{.special = '['}))
                break :blk ExprBase{.brackets = try parseExprList(lex, ']')}
            else return error.Syntax;
        }
    };
    if (!consumed_next) token.* = try lex.take();
    return Expr{.base = base, .suffix = null};
}


// Parse an Expr, starting with the given token. 'token' is updated to
// point to the token after the expression.
fn parseExpr(lex: *Lexer, comptime token: *Token) Error!Expr {
    const expr = try parseExprBase(lex, token);

    var suffix: ?*ExprSuffix = null;
    var suffix_end: ?*ExprSuffix = suffix;

    // Copy the old suffix (if any)
    var esuff = expr.suffix;
    while (esuff) |es| : (esuff = es.next) {
        var n = es.*;
        if (suffix == null) suffix = &n;
        if (suffix_end) |e| e.next = &n;
        suffix_end = &n;
    }

    // And look for our own suffix
    outer: while (true) {
        var suf = blk: {
            if (token.*.eql(.{.special = '.'})) {
                switch (try lex.take()) {
                    .identifier => |s| {
                        token.* = try lex.take();
                        break :blk ExprSuffix{.val = .{.path = s}};
                    },
                    else => return error.Syntax
                }
            } else if (token.*.eql(.{.special = '('})) {
                const lst = try parseExprList(lex, ')');
                token.* = try lex.take();
                break :blk ExprSuffix{.val = .{ .tuple = lst }};
            } else break :outer;
        };
        if (suffix == null) suffix = &suf;
        if (suffix_end) |e| e.next = &suf;
        suffix_end = &suf;
    }
    return Expr{.base = expr.base, .suffix = suffix};
}

// Parse the contents of a group or union block after '{' and up to '}'
fn parseStructBodyContents(comptime lex: *Lexer, out: *StructBody, inunion: bool) Error!void {
    while (true) {
        const t = try lex.take();
        if (t.eql(.{.special = '}'})) break;
        if (!try parseStructBody(lex, out, inunion, t, try lex.take()))
            return error.Syntax;
    }
}

// Parse either a field, union or group; return false if neither was found
fn parseStructBody(comptime lex: *Lexer, out: *StructBody, inunion: bool, first: Token, second: Token) Error!bool {
    // If the second token is an ordinal, assume this is a struct field.
    // It *could* also be a union with an ordinal, but that was
    // deprecated in Capn' Proto v0.3 and is not currently supported by
    // this implementation.
    if (second.eql(.{.special = '@'})) {
        const ord = try lex.take();
        const colon = try lex.take();
        var t = try lex.take();
        const ftype = try parseExpr(lex, &t);
        if (!colon.eql(.{.special = ':'})) return error.Syntax;
        var field = Field{
            .next = out.fields,
            .name = try expectFieldName(first),
            .ord = try expectOrdinal(ord),
            .in_union = inunion,
            .type = ftype,
        };
        out.fields = &field;

        if (t.eql(.{.special = '='})) {
            t = try lex.take();
            field.default = try parseExpr(lex, &t);
        }
        // TODO: annotations
        if (!t.eql(.{.special = ';'})) return error.Syntax;
        return true;

    // Unnamed union
    } else if(first.eql(.{.identifier = "union"}) and second.eql(.{.special = '{'})) {
        if (inunion) return error.InvalidNestedUnion;
        try parseStructBodyContents(lex, out, true);
        return true;
    }

    // Group or named union
    if (!second.eql(.{.special = ':'})) return false;
    const third = try lex.take();
    const isunion = third.eql(.{.identifier = "union"});
    if (isunion or third.eql(.{.identifier = "group"})) {
        var group = Group{
            .next = out.groups,
            .name = try expectFieldName(first),
            .in_union = inunion,
        };
        out.groups = &group;

        // TODO: annotations
        if (!(try lex.take()).eql(.{.special = '{'})) return error.Syntax;
        try parseStructBodyContents(lex, &group.body, isunion);
        return true;
    }

    return false;
}

// Parse a struct after its initial 'struct <name>'
fn parseStruct(comptime lex: *Lexer, out: *Struct) !void {
    var t = try lex.take();
    if (t.eql(.{.special = '@'})) {
        out.uid = try expectUid(try lex.take());
        t = try lex.take();
    }
    // TODO: parameters & annotations

    if (!t.eql(.{.special = '{'})) return error.Syntax;

    while (true) {
        const first = try lex.take();
        if (first.eql(.{.special = '}'})) break;
        const second = try lex.take();

        if (try parseStructBody(lex, &out.body, false, first, second)) {}
        else try parseDecl(lex, &out.decls, first, second);
    }
}

fn parseEnum(lex: *Lexer, out: *Enum) !void {
    var t = try lex.take();
    if (t.eql(.{.special = '@'})) {
        out.uid = try expectUid(try lex.take());
        t = try lex.take();
    }
    // TODO: annotations

    if (!t.eql(.{.special = '{'})) return error.Syntax;

    while (true) {
        const name = try lex.take();
        if (name.eql(.{.special = '}'})) break;
        if (!(try lex.take()).eql(.{.special = '@'})) return error.Syntax;
        const ord = try expectOrdinal(try lex.take());
        // TODO: annotations
        if (!(try lex.take()).eql(.{.special = ';'})) return error.Syntax;

        var enumerant = Enumerant{
            .next = out.enumerants,
            .name = try expectFieldName(name),
            .ord = ord,
        };
        out.enumerants = &enumerant;
    }
}

// Parse a Decl; the first two tokens have already been consumed (required
// for disambiguation in struct & interface context) and are given as
// arguments.
fn parseDecl(comptime lex: *Lexer, decls: *Decls, first: Token, second: Token) Error!void {
    if (first.eql(.{.identifier = "struct"})) {
        var out = Struct{
            .next = decls.structs,
            .name = try expectTypeName(second),
        };
        decls.structs = &out;
        try parseStruct(lex, &out);

    } else if (first.eql(.{.identifier = "enum"})) {
        var out = Enum{
            .next = decls.enums,
            .name = try expectTypeName(second),
        };
        decls.enums = &out;
        try parseEnum(lex, &out);

    } else if (first.eql(.{.identifier = "const"})) {
        unreachable;

    } else if (first.eql(.{.identifier = "interface"})) {
        unreachable;

    } else if (first.eql(.{.identifier = "using"})) {
        unreachable;

    } else if (first.eql(.{.identifier = "annotation"})) {
        unreachable;

    } else return error.Syntax;
}

pub fn parseFile(comptime contents: []const u8) !File {
    var lex = Lexer{.buf = contents };
    var file = File{};
    while (true) {
        const first = (try lex.next()) orelse break;
        const second = try lex.take();

        if (first.eql(.{.special = '@'})) {
            if (file.file_id > 0) return error.MultipleFileIds;
            file.file_id = try expectUid(second);
            if (!(try lex.take()).eql(.{.special = ';'})) return error.Syntax;

        } else try parseDecl(&lex, &file.decls, first, second);
    }
    if (file.file_id == 0) return error.MissingFileId;
    return file;
}

test "parseFile" {
    const exp = std.testing.expect;
    const eql = std.testing.expectEqual;
    const eqs = std.testing.expectEqualStrings;
    @setEvalBranchQuota(100000);
    comptime {
        const ast = try parseFile(
            \\@0x89cc87421cfbe567;
            \\struct X {
            \\  struct Y @0x9cf7c6f4aadcd827 {}
            \\  # Not a valid default value for Int8, just abusing that as an expression parsing test
            \\  fieldname @0 :Int8 = [
            \\      (), (((a,))), b="1" "2" "\3" "\\" "\xfF \1\17\111\777",
            \\      0x"00 11A b fF ",
            \\      ((.Path(arg1, "arg2").b)).name(),
            \\  ];
            \\  struct @1 :Text;  # Not a struct
            \\  union {
            \\      a @2: Data;
            \\      grp :group {
            \\          b @3: Void;
            \\      }
            \\  }
            \\}
            \\enum Enum {
            \\  a @0;
            \\  b @1;
            \\}
        );
        try eql(ast.file_id, 0x89cc87421cfbe567);

        const x = ast.decls.structs.?;
        try eqs(x.name, "X");
        try eql(x.uid, 0);
        try eql(x.next, null);

        const f2 = x.body.fields.?;
        try eqs(f2.name, "a");
        try eql(f2.ord, 2);
        try eqs(f2.type.base.identifier, "Data");
        try eql(f2.default, null);
        try exp(f2.in_union);

        const f1 = f2.next.?;
        try eqs(f1.name, "struct");
        try eql(f1.ord, 1);
        try eqs(f1.type.base.identifier,  "Text");
        try eql(f1.default, null);
        try exp(!f1.in_union);

        const f0 = f1.next.?;
        try eqs(f0.name, "fieldname");
        try eql(f0.ord, 0);
        try eqs(f0.type.base.identifier, "Int8");
        try eql(f0.next, null);
        try exp(!f0.in_union);

        const e0 = f0.default.?.base.brackets.?;
        try eql(e0.expr.base.parens, null);
        try eql(e0.expr.suffix, null);
        try eql(e0.name, null);

        const e1 = e0.next.?;
        try eqs(e1.expr.base.identifier, "a");
        try eql(e1.expr.suffix, null);
        try eql(e1.name, null);

        const e2 = e1.next.?;
        try eqs(e2.expr.base.string, "12\x03\\\xff \x01\x0f\x49\x3f7");
        try eql(e2.expr.suffix, null);
        try eqs(e2.name.?, "b");

        const e3 = e2.next.?;
        try eqs(e3.expr.base.binary, "\x00\x11\xab\xff");
        try eql(e3.expr.suffix, null);
        try eql(e3.name, null);

        const e4 = e3.next.?;
        try eqs(e4.expr.base.root_identifier, "Path");
        try eql(e4.name, null);
        try eql(e4.next, null);
        const e4s0 = e4.expr.suffix.?;
        try eqs(e4s0.val.tuple.?.expr.base.identifier, "arg1");
        try eqs(e4s0.val.tuple.?.next.?.expr.base.string, "arg2");
        try eql(e4s0.val.tuple.?.next.?.next, null);
        const e4s1 = e4s0.next.?;
        try eqs(e4s1.val.path, "b");
        const e4s2 = e4s1.next.?;
        try eqs(e4s2.val.path, "name");
        const e4s3 = e4s2.next.?;
        try eql(e4s3.val.tuple, null);
        try eql(e4s3.next, null);

        const g0 = x.body.groups.?;
        try eqs(g0.name, "grp");
        try eql(g0.next, null);
        try exp(g0.in_union);

        const f3 = g0.body.fields.?;
        try eqs(f3.name, "b");
        try eql(f3.ord, 3);
        try eqs(f3.type.base.identifier, "Void");
        try eql(f3.default, null);
        try exp(!f3.in_union);

        const y = x.decls.structs.?;
        try eqs(y.name, "Y");
        try eql(y.body.fields, null);
        try eql(y.uid, 0x9cf7c6f4aadcd827);
        try eql(y.next, null);

        const en = ast.decls.enums.?;
        try eqs(en.name, "Enum");
        try eql(en.next, null);

        const en1 = en.enumerants.?;
        try eqs(en1.name, "b");
        try eql(en1.ord, 1);

        const en0 = en1.next.?;
        try eqs(en0.name, "a");
        try eql(en0.ord, 0);
        try eql(en0.next, null);
    }
}
