// SPDX-FileCopyrightText: 2022-2023 Yoran Heling <projects@yorhel.nl>
// SPDX-License-Identifier: MIT

// Unlike the lexer in the C++ implementation, this lexer only handles a single
// token at a time without any knowledge about its context. Anything
// context-dependent is handled in the parser.

buf: []const u8,
off: usize = 0,

const std = @import("std");

const Self = @This();
pub const Error = error{UnexpectedEof, Syntax, InvalidEscape, InvalidUtf8};

pub const Token = union(enum) {
    identifier: []const u8,
    string: []const u8, // May contain escapes
    hex_string: []const u8, // May contain whitespace
    integer: i65, // can represent both i64 and u64
    number: f64,
    arrow: void,
    special: u8,

    pub fn eql(a: Token, b: Token) bool {
        if (@as(std.meta.Tag(Token), a) != b) return false;
        return switch (a) {
            .identifier => |x| std.mem.eql(u8, x, b.identifier),
            .string => |x| std.mem.eql(u8, x, b.string),
            .hex_string => |x| std.mem.eql(u8, x, b.hex_string),
            .integer => |x| x == b.integer,
            .number => |x| x == b.number,
            .arrow => true,
            .special => |x| x == b.special,
        };
    }
};

fn peek(s: *Self) u8 {
    return if (s.off >= s.buf.len) 0 else s.buf[s.off];
}

fn incr(s: *Self) void {
    s.off += 1;
}

fn cons(s: *Self) !u8 {
    if (s.off >= s.buf.len) return error.UnexpectedEof;
    defer s.off += 1;
    return s.buf[s.off];
}

fn ws(s: *Self) !void {
    while (true) {
        const ch = s.peek();
        if (ch == '\xef') {
            s.incr();
            if ((try s.cons()) != '\xbb') return error.InvalidUtf8;
            if ((try s.cons()) != '\xbf') return error.InvalidUtf8;

        } else if (ch == '#') {
            s.incr();
            while (true) {
                if ((s.cons() catch break) == '\n') break;
            }

        } else if (std.ascii.isWhitespace(ch)) s.incr()
        else break;
    }
}

// Handles octal/decimal integers and floats.
// Assumption: the byte at 'start' has already been validated to be a correct starting byte.
fn num(s: *Self, start: usize) !Token {
    while (true) {
        const c = s.peek();
        if (!(std.ascii.isDigit(c) or c == '.' or c == 'e' or c == '-')) break;
        s.incr();
    }
    const slice = s.buf[start..s.off];
    const radix = if (slice[0] == '0' or (slice[0] == '-' and slice.len > 1 and slice[1] == '0')) @as(u8, 8) else @as(u8, 10);
    if (std.fmt.parseInt(u64, slice, radix)) |v| return Token{.integer = v}
    else |_| {}
    if (std.fmt.parseInt(i64, slice, radix)) |v| return Token{.integer = v}
    else |_| {}
    if (std.fmt.parseFloat(f64, slice)) |v| return Token{.number = v}
    else |_| {}
    return error.Syntax;
}

pub fn next(s: *Self) !?Token {
    try s.ws();
    const off = s.off;
    const ch = s.cons() catch return null;
    switch (ch) {
        '-' => {
            const c = s.peek();
            if (c == '>') {
                s.incr();
                return Token{.arrow = {}};
            }
            return try s.num(off);
        },
        '0' => {
            var c = s.peek();
            if (c != 'x') return try s.num(off);
            s.incr();
            // hex string
            if (s.peek() == '"') {
                s.incr();
                while (true) {
                    c = s.peek();
                    if (std.ascii.isWhitespace(c) or std.ascii.isHex(c)) s.incr()
                    else if (c == '"') {
                        s.incr();
                        return Token{.hex_string = s.buf[off+3..s.off-1]};
                    } else return error.Syntax;
                }
            }
            // hex integer
            while (true) {
                c = s.peek();
                if (std.ascii.isHex(c)) s.incr()
                else if (std.fmt.parseInt(u64, s.buf[off..s.off], 0)) |v| return Token{.integer = v}
                else |_| return error.Syntax;
            }
        },
        '1'...'9' => return try s.num(off),
        '{', '}', '(', ')', '[', ']', '@', '$', ';', ':', '.', ',', '=' => return Token{.special = ch},
        'a'...'z', 'A'...'Z', '_' => {
            while (true) {
                const c = s.peek();
                if (std.ascii.isAlphanumeric(c) or c == '_') {
                    _ = s.cons() catch unreachable;
                } else return Token{.identifier = s.buf[off..s.off]};
            }
        },
        '"' => {
            while (true) {
                switch (try s.cons()) {
                    '"' => return Token{.string = s.buf[off+1..s.off-1]},
                    '\n' => return error.Syntax,
                    '\\' =>  {
                        switch (try s.cons()) {
                            // Octal escapes may be between 1 and 3 digits,
                            // but no need to bother checking past the
                            // first, we're not interpreting it anyway.
                            'a', 'b', 'f', 'n', 'r', 't', 'v', '\'', '"', '\\', '?', '0'...'7' => {},
                            'x' => {
                                if (!std.ascii.isHex(try s.cons())) return error.InvalidEscape;
                                if (!std.ascii.isHex(try s.cons())) return error.InvalidEscape;
                            },
                            else => return error.InvalidEscape,
                        }
                    },
                    else => {},
                }
            }
        },
        else => return error.Syntax,
    }
}


pub fn take(s: *Self) !Token {
    return (try s.next()) orelse return error.UnexpectedEof;
}


fn runTest() !void {
    var l = Self{.buf =
        \\# this is a comment
        \\@0x89cc87421cfbe567;
        \\ident#another comment
        \\-9223372036854775808 18446744073709551615 -1.5 123.5e-10 0 -0 0.5 0777 -0777
        \\0x""
        \\0x"0123456789
        \\   abcdef
        \\   ABCDEF"
        \\"" "a b c" " \n\\ \t \" \777 \xFF "
        \\{ $Anotation(abc).Something = 3; }
    };
    const tokens = [_]Self.Token{
        .{.special = '@'},
        .{.integer = 0x89cc87421cfbe567},
        .{.special = ';'},
        .{.identifier = "ident"},
        .{.integer = -9223372036854775808},
        .{.integer = 18446744073709551615},
        .{.number = -1.5},
        .{.number = 123.5e-10},
        .{.integer = 0},
        .{.integer = 0},
        .{.number = 0.5},
        .{.integer = 0o777},
        .{.integer = -0o777},
        .{.hex_string = ""},
        .{.hex_string = "0123456789\n   abcdef\n   ABCDEF"},
        .{.string = ""},
        .{.string = "a b c"},
        .{.string = " \\n\\\\ \\t \\\" \\777 \\xFF "},

        .{.special = '{'},
        .{.special = '$'},
        .{.identifier = "Anotation"},
        .{.special = '('},
        .{.identifier = "abc"},
        .{.special = ')'},
        .{.special = '.'},
        .{.identifier = "Something"},
        .{.special = '='},
        .{.integer = 3},
        .{.special = ';'},
        .{.special = '}'},
    };
    for (tokens) |t|
        try std.testing.expect(((try l.next()) orelse unreachable).eql(t));
    try std.testing.expect((try l.next()) == null);
}

test "Lexer" {
    // There seem to be subtle differences between comptime and runtime Zig, so test both.
    try runTest();
    @setEvalBranchQuota(100000);
    comptime try runTest();
}
